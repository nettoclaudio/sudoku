#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from board import Board

SOLUTION_SCORE = 1

def fitness(board):
    return float(1.0 / float((1 + board.calculate_constraints_broken())))

def find_maximum_score(boards):
    scores = list(boards.keys())
    scores.sort()

    maximum_score = scores[len(scores)-1]

    return maximum_score

def climb(boards, score):
    actual_board = boards[score]


def evaluate_boards(boards):
    board_assessment = dict()

    for board in boards:
        score = fitness(board)
        board_assessment[score] = board 

    return board_assessment

def main(filename, population_size):
    board_template = Board.create_board_from_file(filename)
    
    first_board_choices = board_template.generate_random_boards(population_size)

    score_board_dict = evaluate_boards(first_board_choices)    

    while score != SOLUTION_SCORE:
        maximum_score = find_maximum_score(score_board_dict)
        climb(first_board_choices, maximum_score)




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-f', '--filename', help='Filename of board game.',
                        dest='filename', type=str, required=True)

    parser.add_argument('-p', '--population-size', help='Population size.',
                        dest='population_size', type=int, required=True)
    
    args = parser.parse_args()

    main(args.filename, args.population_size)