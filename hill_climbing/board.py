#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import math
import random

class Board(object):

    VALUE_MAX = 9
    VALUE_MIN = 1

    cells = list()

    def __init__(self, cells):
        self.cells = cells

    def constraints_broken_per_line(self, index_row, index_column, value):
        counter = 0

        for k in xrange(index_column+1, self.VALUE_MAX):
            if value == self.cells[index_row][k]:
                counter += 1
        
        return counter

    def constraints_broken_per_column(self, index_row, index_column, value):
        counter = 0

        for k in xrange(index_row+1, self.VALUE_MAX):
            if value == self.cells[k][index_column]:
                counter += 1
        
        return counter
    
    def constraints_broken_per_quadrant(self, index_row, index_column, value):
        counter = 0

        quadrant_size = int(self.VALUE_MAX**(0.5))

        quadrant_bound_row = self.identify_quadrant(index_row, quadrant_size)
        quadrant_bound_column = self.identify_quadrant(index_column, quadrant_size)

        for i in xrange(index_row+1, quadrant_size * quadrant_bound_row):
            for j in xrange(index_column+1, quadrant_size * quadrant_bound_column):
                if self.cells[i][j] == value:
                    counter += 1

        return counter

    def identify_quadrant(self, index, quadrant_size):
        return int(math.ceil((index + 1) / quadrant_size))

    def calculate_constraints_broken(self):
        constraints_broken = 0

        for i in xrange(self.VALUE_MAX):
            for j in xrange(self.VALUE_MAX):
                current_value = self.cells[i][j]

                constraints_broken += self.constraints_broken_per_line(i, j, current_value) + self.constraints_broken_per_column(i, j, current_value) + self.constraints_broken_per_quadrant(i, j, current_value)

        return constraints_broken

    def generate_random_boards(self, size):
        boards = list()

        for it in xrange(size):
            board_cells = list()

            for row in self.cells:
                board_row = list()

                for column in row:
                    
                    if column == 0:
                        column = random.randint(self.VALUE_MIN, self.VALUE_MAX)
                    
                    board_row.append(column)
                
                board_cells.append(board_row)
            
            boards.append(Board(board_cells))

        return boards

    @staticmethod
    def create_board_from_file(filename):
        file_handler = open(filename)

        board_cells = list()

        try:
            reader = csv.reader(file_handler, delimiter=',')
            
            for row in reader:
                board_line = list()
                
                for column in row:
                    column_number = int(column.strip())
                    board_line.append(column_number)
                    
                board_cells.append(board_line)
        finally:
            if file_handler is not None:
                file_handler.close()

        return Board(board_cells)
