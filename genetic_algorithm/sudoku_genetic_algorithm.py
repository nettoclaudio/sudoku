#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random

from sudoku_board import SudokuBoard
from genetic_algorithm import GeneticAlgorithm

class SudokuGeneticAlgorithm(GeneticAlgorithm):

    def __init__(self, sudoku_board_template, population_size, probability_crossover, probability_mutation, elitism=False):
        super(SudokuGeneticAlgorithm, self).__init__(population_size, probability_crossover, probability_mutation)
        
        self.__first_population = sudoku_board_template.generate_random_boards(population_size)
        self.__current_population = self.__first_population
        self.__generation = 1
        self.__board_solution = None
        self.__elitims = elitism

    def check_stop(self):
        
        for board in self.__current_population:
            if self.__is_solution(board):
                self.__board_solution = board

                return True
        
        return False

    def fitness(self, board):
        return float(1.0 / float((1.0 + board.calculate_constraints_broken())))

    def first_population(self):
        return self.__first_population

    def generation(self):
        return self.__generation

    def next_population(self):
        intermediary_population = self.__select_intermediary_population_from_current_population()

        dimension = intermediary_population[0].dimension
        immutable_elements = intermediary_population[0].immutable_elements

        next_population = list()
        pairs = list()

        while len(next_population) < self.population_size:
            index_first = random.randint(0, self.population_size-1)
            index_second = random.randint(0, self.population_size-1)

            while index_first == index_second or (index_first, index_second) in pairs or (index_second, index_first) in pairs:
                index_first = random.randint(0, self.population_size-1)
                index_second = random.randint(0, self.population_size-1)

            pairs.append((index_first, index_second))

            chromosome_one = str(intermediary_population[index_first])
            chromosome_two = str(intermediary_population[index_second])

            crossover_results = self.__crossover(chromosome_one, chromosome_two)

            if crossover_results is not None:
                next_population.append(SudokuBoard.create_square_board_from_binary_text(crossover_results[0], immutable_elements, dimension))
                next_population.append(SudokuBoard.create_square_board_from_binary_text(crossover_results[1], immutable_elements, dimension))
        
        for index in xrange(self.population_size):
            chromosome = str(next_population[index])

            mutation_result = self.__mutation(chromosome)

            if mutation_result is not None:
                next_population[index] = SudokuBoard.create_square_board_from_binary_text(mutation_result, immutable_elements, dimension)

        self.__current_population = next_population
        self.__generation += 1

        return next_population

    def solution(self):
        return self.__board_solution

    def __assessments_propotions(self, assessments):
        assessments_sum = sum(assessments)
        
        return map(lambda assessment: assessment/assessments_sum, assessments)

    def __crossover(self, chromosome_one, chromosome_two):
        if not self.occured_crossover():
            return None
        
        crossing_site = random.randint(0, len(chromosome_one)-1)
        
        new_chromosome_one = chromosome_one[:crossing_site] + chromosome_two[crossing_site:]
        new_chromosome_two = chromosome_two[:crossing_site] + chromosome_one[crossing_site:]

        return new_chromosome_one, new_chromosome_two

    def __is_solution(self, board):
        
        if board.calculate_constraints_broken() == 0:
            return True

        return False

    def __mutation(self, chromosome):
        if not self.occured_mutation():
            return None
        
        for index in xrange(len(chromosome)):

            if random.choice([True, False]):
                chromosome_list = list(chromosome)

                chromosome_list[index] = random.choice(['0', '1'])

        return ''.join(chromosome_list)

    def __normalize_proportions(self, proportions):
        proportions_sum = sum(proportions)

        proportions_ordered = list(proportions)
        proportions_ordered.sort()

        normalized_proportions = list()

        for proportion in proportions_ordered:
            original_index_proportion = proportions.index(proportion)
            
            proportion_by_original_index = (original_index_proportion, proportion)

            normalized_proportions.append(proportion_by_original_index)
            proportions[original_index_proportion] = None

        return normalized_proportions

    def __population_assessments(self, population):
        assessments = list()

        for man in population:
            assessments.append(self.fitness(man))
        
        return assessments

    def __select_intermediary_population_from_current_population(self):
        assessments = self.__population_assessments(self.__current_population)
        
        proportions = self.__assessments_propotions(assessments)
        proportions_sum = sum(proportions)

        normalized_proportions = self.__normalize_proportions(proportions)

        intermediary_population = list()

        if self.__elitims:
            index = assessments.index(max(assessments))
            intermediary_population.append(self.__current_population[index])

        while len(intermediary_population) < self.population_size:
            random_number = random.uniform(0.0, proportions_sum)

            accumulator = 0.0

            for normalized_proportion in normalized_proportions:
                if random_number < (normalized_proportion[1] + accumulator):
                    intermediary_population.append(self.__current_population[normalized_proportion[0]])
                else:
                    accumulator += normalized_proportion[1]

        return intermediary_population