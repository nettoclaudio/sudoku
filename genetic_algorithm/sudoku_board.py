#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math
import random

from board import Board

class SudokuBoard(Board):
    
    def __init__(self, cells, immutable_elements=dict()):
        super(SudokuBoard, self).__init__(cells)

        self.VALUE_MIN = 1
        self.VALUE_MAX = len(self.cells)

        self.__QUADRANT_SIZE = int(math.sqrt(self.VALUE_MAX))
        self.__NUMBER_OF_BITS = int(math.ceil(math.log(self.VALUE_MAX, 2)))

        self.dimension = len(self.cells)

        self.immutable_elements = immutable_elements
    
    def __str__(self):
        code = str('')

        for index_row in xrange(self.dimension):
            for index_column in xrange(self.dimension):
                
                if (index_row, index_column) not in self.immutable_elements:
                    current_element = self.cells[index_row][index_column]
                    
                    code += str('{0:0'+ str(self.__NUMBER_OF_BITS) +'b}').format(current_element)
        
        return code

    def calculate_constraints_broken(self):
        constraints_broken = 0

        for i in xrange(self.VALUE_MAX):
            for j in xrange(self.VALUE_MAX):

                if not self.VALUE_MIN <= self.cells[i][j] <= self.VALUE_MAX:
                   self.cells[i][j] = (self.cells[i][j] % self.dimension) + 1

                constraints_broken += self.__constraints_broken_per_line(i, j)
                constraints_broken += self.__constraints_broken_per_column(i, j)
                constraints_broken += self.__constraints_broken_per_quadrant(i, j)

        return constraints_broken

    def generate_random_boards(self, size):
        boards = list()

        immutable_elements = self.__get_immutable_elements()

        for it in xrange(size):
            board_cells = list()

            for index_row in xrange(self.dimension):
                board_row = list()

                for index_column in xrange(self.dimension):
                    current_element = self.cells[index_row][index_column]

                    if current_element == 0:
                        current_element = random.randint(self.VALUE_MIN, self.VALUE_MAX)

                    board_row.append(current_element)
                
                board_cells.append(board_row)
            
            boards.append(SudokuBoard(board_cells, immutable_elements))

        return boards
    
    def __get_immutable_elements(self):
        immutable_elements = dict()

        for index_row in xrange(self.dimension):
            for index_column in xrange(self.dimension):
                
                current_element = self.cells[index_row][index_column]

                if current_element != 0:
                    identifier_current_element = (index_row, index_column)

                    immutable_elements[identifier_current_element] = current_element

        return immutable_elements

    def __constraints_broken_per_line(self, index_row, index_column):
        counter = 0

        for k in xrange(index_column+1, self.VALUE_MAX):
            if self.cells[index_row][index_column] == self.cells[index_row][k]:
                counter += 1
        
        return counter

    def __constraints_broken_per_column(self, index_row, index_column):
        counter = 0

        for k in xrange(index_row+1, self.VALUE_MAX):
            if self.cells[index_row][index_column] == self.cells[k][index_column]:
                counter += 1
        
        return counter
    
    def __constraints_broken_per_quadrant(self, index_row, index_column):
        counter = 0

        quadrant_bound_row = self.__identify_quadrant(index_row, self.__QUADRANT_SIZE)
        quadrant_bound_column = self.__identify_quadrant(index_column, self.__QUADRANT_SIZE)

        for i in xrange(index_row+1, self.__QUADRANT_SIZE * quadrant_bound_row):
            for j in xrange(index_column+1, self.__QUADRANT_SIZE * quadrant_bound_column):
                if self.cells[index_row][index_column] == self.cells[i][j]:
                    counter += 1

        return counter

    def __identify_quadrant(self, index, quadrant_size):
        return int(math.ceil((index + 1) / quadrant_size))
    
    @staticmethod
    def create_square_board_from_binary_text(binary_text, immutable_elements=dict(), dimension=9):
        numbers = list()

        number_of_bits = int(math.ceil(math.log(dimension, 2)))

        for index in xrange(0, len(binary_text), number_of_bits):
            binary_number = binary_text[index:index+number_of_bits]

            number = int(binary_number, 2)

            numbers.append(number)
        
        numbers.reverse()

        cells = list()

        for index_row in xrange(dimension):
            row = list()

            for index_column in xrange(dimension):
                identifier_current_element = (index_row, index_column)

                if identifier_current_element in immutable_elements:
                    current_element = immutable_elements[identifier_current_element]
                else:
                    current_element = numbers.pop()
                
                if len(row) == dimension:
                    cells.append(row)
                    row = list()

                row.append(current_element)
            cells.append(row)

        return SudokuBoard(cells, immutable_elements)