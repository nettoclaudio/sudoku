#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv

class Board(object):

    def __init__(self, cells):
        self.cells = cells

    def __repr__(self):
        return '{0}.{1}({2})'.format(self.__module__, type(self).__name__, self.cells)

    @staticmethod
    def create_board_from_file(filename):
        file_handler = open(filename)

        board_cells = list()

        try:
            reader = csv.reader(file_handler, delimiter=',')
            
            for row in reader:
                board_line = list()
                
                for column in row:
                    column_number = int(column.strip())
                    board_line.append(column_number)
                    
                board_cells.append(board_line)
        finally:
            if file_handler is not None:
                file_handler.close()

        return Board(board_cells)
