#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random

class GeneticAlgorithm(object):

    def __init__(self, population_size, probability_crossover, probability_mutation):
        self.population_size = population_size
        self.probability_crossover = probability_crossover
        self.probability_mutation = probability_mutation

    def check_stop(self):
        raise NotImplementedError('Subclasses must override it.')

    def first_population(self):
        raise NotImplementedError('Subclasses must override it.')

    def fitness(self):
        raise NotImplementedError('Subclasses must override it.')
    
    def next_population(self):
        raise NotImplementedError('Subclasses must override it.')

    def occured_mutation(self):
        return random.random() < self.probability_mutation
    
    def occured_crossover(self):
        return random.random() < self.probability_crossover