#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import time

from board import Board
from sudoku_genetic_algorithm import SudokuGeneticAlgorithm
from sudoku_board import SudokuBoard

def main(filename, population_size, probability_crossover, probability_mutation, elitism):
    generic_board = Board.create_board_from_file(filename)

    sga = SudokuGeneticAlgorithm(SudokuBoard(generic_board.cells), 
                                 population_size, 
                                 probability_crossover, 
                                 probability_mutation,
                                 elitism)

    time_start = time.time()

    while not sga.check_stop():
        current_population = sga.next_population()

        current_assesments = map(lambda board: sga.fitness(board), current_population)

        best_assesment  = max(current_assesments)
        worse_assesment = min(current_assesments)

        print
        print "[INFO] Generation: %d" % sga.generation()
        print "[INFO] Best assesment: %f" % best_assesment
        print "[INFO] Worse assesment: %f" % worse_assesment
        print "[INFO] Difference between the best and the worse: %f" % (best_assesment - worse_assesment)

    time_end = time.time()

    print "[INFO] The solution has been found."
    print "[INFO] Elapsed time: %f seconds." % (time_end - time_start)
    print "[INFO] Generations until solution: %d." % sga.generation()
    print "[INFO] Solution: %s." % sga.solution().cells

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-f', '--filename', help='Filename of board game.',
                        dest='filename', type=str, required=True)

    parser.add_argument('-p', '--population-size', help='Population size.',
                        dest='population_size', type=int, required=True)
    
    parser.add_argument('-c', '--probability-crossover', help='Probability of crossover.',
                        dest='probability_crossover', type=float, required=True)

    parser.add_argument('-m', '--probability-mutation', help='Probability of mutation.',
                        dest='probability_mutation', type=float, required=True)

    parser.add_argument('-e', '--with-elitism', help="Activate elitism method.",
                        dest='elitism', action='store_true', required=False)

    args = parser.parse_args()

    main(args.filename,
         args.population_size,
         args.probability_crossover,
         args.probability_mutation,
         args.elitism)
